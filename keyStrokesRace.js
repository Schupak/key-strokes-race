const jwt = require('jsonwebtoken');
const texts = require('./data/texts.json');

class KeyStrokesRace {
    constructor({
        io
    }) {
        this.io = io;
        this.defaultRaceTime = 15;
        this.defaultWaitTime = 10;
        this.defaultWaitingCountdown = this.defaultRaceTime + this.defaultWaitTime;
        this.waitingCountdown = this.defaultWaitingCountdown;
        this.timer = null;
        this.raceStarted = false;
        this.currentText = this.getRaceText();

    }

    startRace() {
        this.currentText = this.getRaceText();
        this.initRacers();
        this.io.to('racing').emit('startRace', {
            racers: this.racers
        });
        this.toggleRaceState();
    }

    stopRace() {
        this.toggleRaceState();
        const afterRaceClients = this.io.sockets.adapter.rooms['after-race'].sockets;
        Object.keys(afterRaceClients).forEach(socketId => {
            const socket = this.io.sockets.connected[socketId];
            socket.leave('after-race');
            socket.join('waiting');
        });
    }

    startPlaying() {
        this.waitingCountdown = 10;
        this.io.to('waiting').emit('waiting-countdown', {
            time: this.waitingCountdown
        });

        this.timer = setInterval(() => {
            this.waitingCountdown--;
            if (this.waitingCountdown === 0) {
                this.startRace();
                this.resetCountdown();
            }

            this.racingCountdown = this.waitingCountdown - this.defaultWaitTime;
            if (this.racingCountdown === 0 && this.raceStarted) {
                this.stopRace();
            }

            this.io.to('waiting').to('after-race').emit('waiting-countdown', {
                time: this.waitingCountdown
            });
            this.io.to('racing').emit('racing-countdown', {
                time: this.racingCountdown
            });
        }, 1000);
    };

    stopPlaying() {
        clearInterval(this.timer);
        this.resetCountdown();
        this.raceStarted = false;
    };

    initRacers() {
        const waitingClients = this.io.sockets.adapter.rooms['waiting'].sockets;
        this.racers = Object.keys(waitingClients).map(socketId => {
            const socket = this.io.sockets.connected[socketId];
            const {
                username
            } = this.getUserFromJWT(socket.handshake.query.token);
            socket.leave('waiting');
            socket.join('racing');

            socket.on('char-entered', ({
                token
            }) => {
                jwt.verify(token, 'secret', (error, {
                    username
                }) => {
                    if (!error) {
                        const userIndex = this.racers.findIndex(user => user.username === username);
                        this.racers[userIndex].enteredSymbols++;

                        this.io.to('racing').to('after-race').emit('racers-data', {
                            racers: this.racers
                        });
                    } else {
                        console.log(error);
                    }
                });
            });

            socket.on('finished', ({
                token
            }) => {
                jwt.verify(token, 'secret', (error, {
                    username
                }) => {
                    if (!error) {
                        const userIndex = this.racers.findIndex(user => user.username === username);
                        this.racers[userIndex].usedTime = this.racingCountdown;

                        this.io.to('racing').to('after-race').emit('racers-data', {
                            racers: this.racers
                        });
                        socket.leave('racing');
                        socket.join('after-race');
                        socket.emit('end-race');
                    } else {
                        console.log(error);
                    }
                });
            });

            socket.on('disconnect', () => {
                if (!this.io.engine.clientsCount) {
                    this.racers = null;
                } else {
                    const userIndex = this.racers.findIndex(user => user.username === username);
                    this.racers[userIndex].leaved = true;
                    this.io.to('racing').emit('racers-data', {
                        racers: this.racers
                    });
                }
            });

            return {
                username,
                usedTime: 0,
                enteredSymbols: 0,
                leaved: false
            };
        });
    }

    getRaceText() {
        return texts[Math.floor(Math.random() * texts.length)];
    }

    toggleRaceState() {
        this.raceStarted = !this.raceStarted;
    }

    resetCountdown() {
        this.waitingCountdown = this.defaultWaitingCountdown;
    };

    getUserFromJWT(token) {
        const {
            username,
            email
        } = jwt.decode(token);
        return {
            username,
            email
        };
    }
}

module.exports = {
    KeyStrokesRace
};