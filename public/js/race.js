window.onload = () => {


    const jwt = localStorage.getItem('jwt');

    if (!jwt) {
        location.replace('/login');
    } else {
        const socket = io('http://localhost:5500', {
            query: {
                token: jwt
            }
        });
        let raceText, charCount = 0;
        const countdownContainer = document.body.querySelector('.countdown');
        const raceTimer = document.body.querySelector('.race-timer');
        const currentCharContainer = document.body.querySelector('.current-char');
        const racersTable = document.body.querySelector('.race-racers');
        const raceContainer = document.body.querySelector('.race-container');
        const raceTextContainer = document.body.querySelector('.race-text');

        const generateRacersTableRows = (racers, textLength) => {
            return racers.map(({
                username,
                enteredSymbols,
                leaved
            }) => {
                const tr = document.createElement('tr');
                const tdUsername = document.createElement('td');
                const tdProgressBar = document.createElement('td');
                const progressBarContainer = document.createElement('div');
                const progressBar = document.createElement('div');

                progressBarContainer.className = 'progress-bar-container';
                progressBar.className = 'progress-bar';
                progressBar.style.width = `${Math.round(enteredSymbols / textLength * 100)}%`;
                progressBarContainer.appendChild(progressBar);
                tdProgressBar.appendChild(progressBarContainer);
                tdUsername.textContent = username;
                if (leaved) {
                    tdUsername.innerText = 'leaved-participant';
                    tdUsername.innerText = `${username} offline`;
                }


                tr.appendChild(tdUsername);
                tr.appendChild(tdProgressBar);
                return tr;
            })
        };

        const showRaceLayout = (racers, raceText) => {
            raceContainer.insertBefore(racersTable, raceTextContainer);
            countdownContainer.style.display = 'none';
            raceContainer.style.display = 'block';
            raceTextContainer.textContent = raceText;
            clearRacersTable();
            generateRacersTableRows(racers, raceText.length).forEach(row => {
                racersTable.tBodies[0].appendChild(row);
            });
        };


        const hideRaceLayout = () => {
            countdownContainer.style.display = 'block';
            raceContainer.style.display = 'none';
            document.body.querySelector('.container').insertBefore(racersTable, countdownContainer);

        };


        const clearRacersTable = () => {
            [...racersTable.tBodies[0].rows].forEach(row => {
                row.parentElement.removeChild(row);
            });
        };


        fetch('/api/text', {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${jwt}`
            }
        }).then(res => {
            res.json().then(body => {
                raceText = body.text;
            })
        });

        socket.on('racing-countdown', ({
            time
        }) => {
            raceTimer.textContent = `${time}s left`;
        });

        socket.on('waiting-countdown', ({
            time
        }) => {
            countdownContainer.textContent = `Wait until next race start: ${time}s`;
        });


        const highlightAtIndex = (index) => {
            const raceTextContainer = document.body.querySelector('.race-text');
            raceTextContainer.innerHTML = `<span class="previous-chars-highlight">${raceText.slice(0, index)}</span><span class="current-char-highlight">${raceText[index]}</span>${raceText.slice(index + 1)}`;
        };

        const keypressHandler = (event) => {
            const {
                keyCode
            } = event;
            if (String.fromCharCode(keyCode) === raceText[charCount]) {
                const progressBar = document.body.querySelector('.progress-bar');

                socket.emit('char-entered', {
                    token: jwt
                });
                charCount++;

                if (raceText.length === charCount) {
                    socket.emit('finished', {
                        token: jwt
                    });
                }

                progressBar.style.width = `${Math.round(charCount / raceText.length * 100)}%`;
                highlightAtIndex(charCount);
            }
        };

        socket.on('startRace', ({
            racers
        }) => {
            currentCharContainer.style.display = 'block'
            showRaceLayout(racers, raceText);
            highlightAtIndex(0);
            document.body.addEventListener('keypress', keypressHandler);
        });

        socket.on('end-race', () => {
            hideRaceLayout();
            document.body.removeEventListener('keypress', keypressHandler);
        });

        socket.on('racers-data', ({
            racers
        }) => {
            clearRacersTable();

            let tableRows = generateRacersTableRows(racers, raceText.length);
            [...tableRows].sort((rowA, rowB) => {
                const userA = racers.find(user => user.username === rowA.cells[0].textContent);
                const userB = racers.find(user => user.username === rowB.cells[0].textContent);

                if (userA.leaved) return 1;
                if (userB.leaved) return -1;
                const symbolsDiff = userB.enteredSymbols - userA.enteredSymbols;
                if (!symbolsDiff) {
                    return userA.usedTime - userB.usedTime;
                }
                return symbolsDiff;
            }).forEach(row => {
                racersTable.tBodies[0].appendChild(row);
            });
        });
    }
};