const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const bodyParser = require('body-parser');
const path = require('path');
const passport = require('passport');
const jwt = require('jsonwebtoken');

const users = require('./data/users.json');
const {
    KeyStrokesRace
} = require('./keyStrokesRace');

require('./config/passport.config');

server.listen(5500);

const options = {
    io
};
const keyStrokes = new KeyStrokesRace(options);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/index.html'));
});

app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/login.html'));
});

app.post('/login', (req, res) => {
    const loggingUser = req.body;
    const userInDB = users.find(user => {
        return user.email === loggingUser.email && user.password === loggingUser.password;
    });

    if (userInDB) {
        const token = jwt.sign(userInDB, 'secret');
        res.status(200).json({
            auth: true,
            token
        });
    } else {
        res.status(401).json({
            auth: false
        });
    }
});

app.get('/race', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/race.html'));
});

app.get('/api/text', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    res.status(200).send({
        text: keyStrokes.currentText
    });
});

io.on('connection', (socket) => {
    socket.emit('countdown', {
        time: keyStrokes.waitingCountdown
    });
    socket.join('waiting');

    if (io.engine.clientsCount === 1) {
        keyStrokes.startPlaying();
    }

    socket.on('disconnect', () => {

        if (!io.engine.clientsCount) {
            keyStrokes.stopPlaying();
        }
    });
});